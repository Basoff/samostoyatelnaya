#include <iostream>
#include "Textfile.h"
using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	char* tes1 = "test1.dat";
	char* tes2 = "test2.dat";
	TextFile test1 (tes1);
	TextFile test2 (tes2);
	test1.Add(test2);
	return 0;
}