#ifndef _TEXTFILE_H
#define _TEXTFILE_H

#include <fstream>
using namespace std;
class TextFileException{};

class TextFile
{
	public:
	ifstream in;
	ofstream out;
	char address;
	TextFile(char* name);
	~TextFile();
	TextFile& Add (TextFile& input);
};

#endif